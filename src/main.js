// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'

Vue.config.productionTip = false

// Components
import BannerStatic from '@/components/banner/static'
import BannerSlider from '@/components/banner/slider'
import AppHeader from '@/components/app-header'
import AppFooter from '@/components/app-footer'
import AppSidebar from '@/components/app-sidebar'
import GoToTop from '@/components/go-to-top'
import VueBreadcrumbs from 'vue-breadcrumbs'

// Components init

const components = {
    App,
    BannerStatic,
    BannerSlider,
    AppHeader,
    AppFooter,
    AppSidebar,
    GoToTop,
    VueBreadcrumbs,
}

Object.keys(components).forEach(key => {
    Vue.component(key, components[key])
})

// Plugins init

import VueProgressBar from 'vue-progressbar'
import VueScrollTo from 'vue-scrollto'
import VueHead from 'vue-head'
import Scrollspy from 'vue2-scrollspy'

/**
 * VUE NOTIFY
 * https://github.com/se-panfilov/vue-notifications
 */


    import VueNotifications from 'vue-notifications'

    import iziToast from 'izitoast'// https://github.com/dolce/iziToast
    import 'izitoast/dist/css/iziToast.min.css'

    function toast ({title, message, type, timeout, cb}) {
      if (type === VueNotifications.types.warn) type = 'warning'
      return iziToast[type]({title, message, timeout: 5000})
    }

    const options = {
      success: toast,
      error: toast,
      info: toast,
      warn: toast,
    }

    Vue.use(VueNotifications, options)

/**
 * END VUE NOTIFY
 */


Vue.use(Scrollspy);

Vue.use(VueProgressBar, {
    color: '#017f1e',
    failedColor: 'red',
    height: '3px'
})

Vue.use(VueScrollTo)

Vue.use(VueHead);

// Global variables

Vue.prototype.API_LOCATION = process.env.API_LOCATION;
Vue.prototype.API_HOST = process.env.API_HOST;

// Store
import store from './store/store'

/* eslint-disable no-new */
new Vue({
    el: '#app',
    router,
    store,
    components,
    template: '<App/>',
})