import Vue              from 'vue'
import Router           from 'vue-router'
import BasicLayout      from '@/layouts/Basic'
import Home             from '@/pages/home'

// About
import About            from '@/pages/about'

import Gallery          from '@/pages/gallery'

import Video          from '@/pages/video'

// Tuyen dung
import Recruitment      from '@/pages/recruitment'
import SingleRecruitment from '@/pages/single-recruitment'

import Contact          from '@/pages/contact'

// Du an
import Project          from '@/pages/project'
import SingleProject    from '@/pages/single-project'

// Tin tuc
import News         from '@/pages/news'
import SingleNews   from '@/pages/single-news'

import Support      from '@/pages/support'

import PageNotFound from '@/pages/404'

Vue.use(Router)

export default new Router({
    mode: 'history',
    routes: [
        
        {
            path: '/',
            name: 'Frontend',
            component: BasicLayout,
            children: [
                {
                    path: '/',
                    name: "Home",
                    component: Home
                },
    	        {
    	            path: '/gioi-thieu/:slug?',
    		        name: 'About',
    		        component: About
    	        },
                {
                    path: '/thu-vien',
                    name: 'Video',
                    component: Video
                },
                {
                    path: '/tuyen-dung/vi-tri/:slug',
                    name: 'SingleRecruitment',
                    component: SingleRecruitment
                },
                {
                    path: '/tuyen-dung/:slug?',
                    name: 'Recruitment',
                    component: Recruitment,
                },
                {
                    path: '/du-an',
                    name: 'AllProject',
                    component: Project,
                    props: { page: 1 }
                },
                {
                    path: '/du-an/:category_slug',
                    name: "CategoryProject",
                    component: Project,
                    props: { page: 1 }
                },
                {
                    path: '/du-an/:category_slug/:project_slug',
                    name: "SingleProject",
                    component: SingleProject,
                },
                {
                    path: '/tin-tuc',
                    name: 'AllNews',
                    component: News,
                },
                {
                    path: '/tin-tuc/:category_slug',
                    name: 'CategoryNews',
                    component: News,
                },
                {
                    path: '/tin-tuc/:category_slug/:news_id-:news_slug',
                    name: 'SingleNews',
                    component: SingleNews,
                },
                {
                    path: '/lien-he',
                    name: 'Contact',
                    component: Contact
                },

                {
                    path: '/:slug',
                    name: 'Support',
                    component: Support
                },
      
            ]
        },
        // { 
        //     path: "*", 
        //     name: "404",
        //     component: PageNotFound ,
        // },
    ],
    scrollBehavior (to, from, savedPosition) {
        return { x: 0, y: 0 }
    },
    linkExactActiveClass: "selected",
})