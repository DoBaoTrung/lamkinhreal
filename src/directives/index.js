import Vue from 'vue'

import '@/assets/Js/jquery.matchHeight-min.js'

export const MatchHeight = {
    // When the bound element is inserted into the DOM...
    inserted: function(el, binding) {
        let elements = binding.value.el;
        console.log(elements);

        if (elements)
	        $.each(elements, function(index, element) {
	        	$(el).find(element).matchHeight();
	        });
	   	else
        	console.warn("No elements were passed.");
    }
};


