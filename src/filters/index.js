export const excerpt = (value) => {
	if (!value) return ''
	    value = value.toString();

	return value.replace(/\s+/g,' ');
}

export const number_format = (x) => {
	x = parseInt(x);

	if (isNaN(x)) return 0;

	var sep;
	var grp;

    var sx = (''+x).split('.'), s = '', i, j;
    sep || (sep = ','); // default seperator
    grp || grp === 0 || (grp = 3); // default grouping
    i = sx[0].length;
    while (i > grp) {
        j = i - grp;
        s = sep + sx[0].slice(j, i) + s;
        i = j;
    }
    s = sx[0].slice(0, i) + s;
    sx[0] = s;
    return sx.join('.');

}