'use strict'
const merge = require('webpack-merge')
const prodEnv = require('./prod.env')

const API_HOST 			= 'http://lamkinhreal.com';
const API_LOCATION		= API_HOST + '/api/';

module.exports = merge(prodEnv, {
  NODE_ENV: '"development"',
  API_LOCATION: '"' + API_LOCATION + '"',
  API_HOST: '"' + API_HOST + '"',
})
