'use strict'

const API_HOST 			=  'http://lamkinhreal.com';
const API_LOCATION		= API_HOST + '/api/';

module.exports = {
  NODE_ENV: '"production"',
  API_LOCATION: '"' + API_LOCATION + '"',
  API_HOST: '"' + API_HOST + '"',
}
